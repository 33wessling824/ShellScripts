# ShellScripts

This is a collection of scripts I use or have been using. Some are from my workstation others are
from my server(s).

Those systems are or have been:
* ArchLinux
* Clear Linux
* Debian
* FreeBSD
* Raspbian
* RedHat
* Ubuntu
* others

They won't work on all of the mentioned systems!

**Please! DO NOT use these files without checking and understandig!** These scripts could be good examples, *BUT no more!*

---

*If scripts work together it's mentioned in the header of the files.*

## Scripts

### HPT630/...

HPT630 is a Ubuntu-based host with Docker. These scripts are called by `crontab` to get backups.

#### backupPaperless

#### pushWiki

### old/...

_All files inside this directory are NOT anymore in use!_

#### renameRAW

Images coming from my camera renamed and numbered as I wish. EXIF-Tags for 'author', 'artist' and 'copyright' are added.

#### setTwoDNS

Update your IP at a dynamic DNS service (here TwoDNS)

#### switchConkyConf

Use two configuraion files with my conky installation. Sometimes I use my notebook with a big monitor.

#### update

Combine the nesseccary update statements of a Debian based system.

### OMV/...

OMV is an Debian-based host, with OpenMediaVault on it.

#### keepYoungest

Is regulary called with crontab. It keeps the 4 youngest / newest files in the given directory.

### ZenBook/...

ZenBook is my working horse. An Asus UX330 with Manjaro linux.